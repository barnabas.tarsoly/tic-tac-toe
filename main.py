
import os
import random
import time
class Table:
    def __init__(self, size):
        self.map=[]
        self.size = size
        for x in range(size):
            temp = []
            for _ in range(size):
                temp.append(" ")
            self.map.append(temp)
        self.win = None
        self.win_num = 5

    def draw_table(self):
        os.system("cls")
        for x in range(self.size):
            for y in range(self.size):
                if y == 0:
                    print(f"| {self.map[x][y]} |",end="")
                else:
                    print(f" {self.map[x][y]} |",end="")
            print()
            if x < self.size - 1:
                print("----" + "+---" * (self.size - 1) + "-")

    def check_player(self, chr, row, column):
        temp_row = row-1
        temp_colomn = column-1
        number = 1
        break_for = False
        for x in range(8):
            if break_for:
                break

            if x == 0:
                while True:
                    try:
                        temp_row+=1
                        if self.map[temp_row][temp_colomn] == chr:
                            number += 1
                            if number == self.win_num:

                                self.win = chr
                                break_for = True
                                break
                        else:

                            break
                    except:

                        break
            elif x == 1:
                temp_row = row - 1
                temp_colomn = column - 1
                while True:
                    try:
                        temp_row -= 1
                        if self.map[temp_row][temp_colomn] == chr:
                            number += 1
                            if number == self.win_num:

                                self.win = chr
                                break_for = True
                                break
                        else:

                            break
                    except:

                        break
            elif x == 2:
                number = 1
                temp_row = row - 1
                temp_colomn = column - 1
                while True:
                    try:
                        temp_colomn += 1
                        if self.map[temp_row][temp_colomn] == chr:
                            number += 1
                            if number == self.win_num:

                                self.win = chr
                                break_for = True
                                break
                        else:

                            break
                    except:

                        break
            elif x == 3:
                temp_row = row - 1
                temp_colomn = column - 1
                while True:
                    try:
                        temp_colomn -= 1
                        if self.map[temp_row][temp_colomn] == chr:
                            number += 1
                            if number == self.win_num:

                                self.win = chr
                                break_for = True
                                break
                        else:

                            break
                    except:

                        break
            elif x == 4:
                number = 1
                temp_row = row - 1
                temp_colomn = column - 1
                while True:
                    try:
                        temp_colomn -= 1
                        temp_row += 1
                        if self.map[temp_row][temp_colomn] == chr:

                            number += 1
                            if number == self.win_num:

                                self.win = chr
                                break_for = True
                                break
                        else:
                            number = 1
                            break
                    except:
                        number = 1
                        break
            elif x == 5:

                temp_row = row - 1
                temp_colomn = column - 1
                while True:
                    try:
                        temp_colomn += 1
                        temp_row -= 1
                        if self.map[temp_row][temp_colomn] == chr:
                            number += 1
                            if number == self.win_num:

                                self.win = chr
                                break_for = True
                                break
                        else:

                            break
                    except:

                        break
            elif x == 6:
                number = 1
                temp_row = row - 1
                temp_colomn = column - 1
                while True:
                    try:
                        temp_colomn += 1
                        temp_row += 1
                        if self.map[temp_row][temp_colomn] == chr:
                            number += 1
                            if number == self.win_num:

                                self.win = chr
                                break_for = True
                                break
                        else:

                            break
                    except:

                        break
            elif x == 7:

                temp_row = row - 1
                temp_colomn = column - 1
                while True:
                    try:
                        temp_colomn -= 1
                        temp_row -= 1
                        if self.map[temp_row][temp_colomn] == chr:

                            number += 1
                            if number == self.win_num:

                                self.win = chr
                                break_for = True
                                break
                        else:


                            break
                    except:


                        break
    def change_map(self, chr, row, column):
        if self.map[row-1][column-1] == " ":

            self.map[row-1][column-1] = chr
            self.check_player(chr, row, column)
            return 0
        else:

            return 1









def number_check(num):
    try:
        while int(num) < 5:
            os.system("cls")
            print("WRONG NUMBER")
            num = input("Table size (MIN 5x5):")
    except:
        os.system("cls")
        print("NOT A NUMBER")
        num = input("Table size (MIN 5x5):")
        number_check(num)

    return int(num)

def main():
    os.system("cls")
    temp_size = input("Table size (MIN 5x5):")
    TABLE_SIZE = number_check(temp_size)
    player_number = input("Player number (1: Player vs random, 2: PvP): ")
    table = Table(TABLE_SIZE)
    inp = ""
    player = "X"
    table.draw_table()
    while table.win == None:
        #inp = input("PLAYER: ")

        if inp == "q":
            break
        print("PLAYER: ",player)
        if player_number == "1":
            if player == "X":
                row = int(input("ROW: "))
                column = int(input("COLUMN: "))
            else:
                row = random.randint(1,TABLE_SIZE)
                column = random.randint(1, TABLE_SIZE)
        elif player_number == "2":
            row = int(input("ROW: "))
            column = int(input("COLUMN: "))
        else:
            row = random.randint(1, TABLE_SIZE)
            column = random.randint(1, TABLE_SIZE)
        if row <= TABLE_SIZE and column <= TABLE_SIZE:

            map = table.change_map(player, row, column)
        else:
            map = 1

        table.draw_table()

        if map:
            print("WRONG FIELD")
        if player == "X":
            player = "O"
        else:
            player = "X"
        if player_number == "3":
            time.sleep(0.3)
    print(f"WON: {table.win}")

if __name__ == '__main__':
    main()


